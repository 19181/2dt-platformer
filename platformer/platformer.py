import utils
import buttonActions
import AudioEngine

import arcade

from math import floor
from threading import Thread

from pynput.mouse import Controller
mouse = Controller()

import ctypes

## ---- CONSTANTS ---- ##

# These constants are the minimum distances that need to be between the player and the edge of the viewport
# They will be used in calculating when and by how much to scroll the vieport
PLAYER_SPRITE_LR_MARGIN = 300
PLAYER_SPRITE_BOTTOM_MARGIN = 255
PLAYER_SPRITE_TOP_MARGIN = 200

# How many frames we should go through before we update the animation
FRAMES_PER_TEXTURE = 6

# This needs to be equal to the amount of levels (not including level 0)
LEVEL_COUNT = 6

# Main class which contains the methods that arcade will call to run the game
class Platformer(arcade.Window):
    """Main application class."""
    
    def __init__(self, width, height, title):
        # Setup the window
        super().__init__(width, height, title)

        # Center the window on the screen
        super().set_location(50, 50)

        # Open the file that contains the players current level and their highest level
        # The current level is a number stored on line 1, and the highest level is stored on line 2
        with open("userData.txt", "a+") as userData:
            # Set the pointer to the start of the file
            userData.seek(0, 0)
            firstLine = userData.readline()

            # If the first line is empty then something has previously deleted the file or its contents, so 
            # we need to set the level to default (level 1) and write it into the file.
            # The music state also needs to be written to the file
            if(not firstLine):
                self.level = 1
                self.highestLevel = 1
                self.playMusic = True
                userData.write("1\n1\nTrue")
            else:
                #print(userData.readline())
                # If it isn't empty then the first line will have the users current level
                self.level = int(firstLine)
                # The second line will have the users highest level
                self.highestLevel = int(userData.readline())
                # The third line will hold a boolean, this is whether the user wants music on or not
                # Since it is in string format we need to convert it to a boolean with a simple check
                self.playMusic = userData.readline() == "True"

        # Set the background color to a light blue
        arcade.set_background_color([181, 227, 255])

        # Once the game has loaded, this will be a list of dictionaries containing each of the layers for that level
        self.levels = []

        # self.movementStates will hold the states for each of the movement directions. If the value is "pressed",
        # then some buttons are being pressed to move the player in that direction.
        # keyStates will hold the individual states of each of they keys. We will use this to determine the states
        # for self.movementStates using the utils.resolveMovementStates() function
        #
        # The numbers can be found here -> https://arcade.academy/arcade.key.html
        self.movementStates = {"up": "not_pressed", "down": "not_pressed", "left": "not_pressed", "right": "not_pressed"}
        self.keyStates = {65362: "not_pressed", 65364: "not_pressed", 65361: "not_pressed", 65363: "not_pressed",
                          119: "not_pressed", 115: "not_pressed", 97: "not_pressed", 100: "not_pressed",
                          32: "not_pressed"}

        # These will hold the state of the mouse and will be updated whenever something changes
        self.mouse_x = mouse.position[0] - super().get_location()[0]
        self.mouse_y = SCREEN_HEIGHT - (mouse.position[1] - super().get_location()[1]) - 1
        self.mousePressed = False

        # self.screenType keeps track of the current screen the game is on. This changes based on what the user does
        #
        # ----- Possible Values ------
        # 0 - Main menu, displayed after the game finishes loading
        # 1 - normal game screen, what users see when they are actually playing the game
        # 2 - Level selection screen
        # 3 - Loading screen
        # 4 - Tutorial screen. This screen will be removed once I can make a tutorial map.
        self.screenType = 3
        
        # These will keep track of the viewport location and will be used to determine when to scroll the viewport
        self.viewportBottom = 0
        self.viewportLeft = 0

        # Three states that the game can be in. We use this to decide when to draw the respawn, pause or
        # are-you-sure dialog boxes/menus
        self.paused = False
        self.playerDead = False
        self.displayAreYouSure = False

        # Since there are two reasons for the are-you-sure menu to be shown, this will help decide where
        # the user wants to go if they click yes. It will either hold "main_menu" , "restart_level" or "exit_game"
        self.displayAreYouSure_Reason = ""

        # These will be used to determine if the player is dying and we need to flash the texture on and off
        self.playerDying = False
        self.totalDyingFrames = 0

        # To hold the fraction of the game's loading progress. Used in loadAssets() and loadingScreen()
        self.loadingProgress = 0

        # This list will hold all of the button buttons for the current menu/screen. Each item will be a
        # dictionary with the values "button", "function" and "drawOnly". The data for "button" will be a  list
        # containing center_x, center_y, width, height and text of each button.
        # Right now I don't actually need the width and the height because all of the buttons are the same, but
        # I'm adding them in now so if I do make different size buttons later, I wont need to change this list everywhere.
        # The data for "function" will be the function object from buttonActions that this button needs to call.
        # "drawOnly" will be a boolean for determining if the button is allowed to be clicked. We need this for
        # places like the main menu, where there's a popup and the other buttons can still be seen in the background.
        self.buttons = []

        # If these variables are used for most physics things instead of using the physics engine to detect
        # collisions, it results in a slight speed increase
        self.onLadder = False
        self.onGround = True

        # Create the audio engine instance. We need this because the audio engine handles everything to do with
        # sound, and so we need to make calls to it 
        self.audioEngine = AudioEngine.AudioEngine()

    def setupLevel(self):
        """
        This is where we setup the level
        The terrain for the current level is passed to the physics engine, and any coordinates are reset
        """

        # Retrieve the dictionary of layers for the current level
        level = self.levels[self.level]

        #+ Platforms
        self.terrainList = level["platforms"]
        
        # Background tiles that the player doesn't actually touch
        self.backgroundList = level["background"]

        # The layer that holds all the ladders. We will need to pass this to the physics engine to enable
        # the player to climb ladders
        self.ladderList = level["ladders"]

        # Layer that has all of the dangerous tiles in it
        self.dangerList = level["danger"]

        # The layer that holds all of the objects
        self.objectLayer = level["objects"]

        # Setup the physics engine with a gravity value of 0.5 and multijump disabled
        self.physicsEngine = arcade.PhysicsEnginePlatformer(self.playerSprite, self.terrainList, 0.5, self.ladderList)
        self.physicsEngine.disable_multi_jump()

        # If the level is bigger than the players highest level, then they have unlocked a new level so we need to change their highest level.
        if(self.level > self.highestLevel):
            self.highestLevel = self.level

        # Write the new data into the file
        utils.writeUserData(self)

        # if the map has a set background color
        if(self.objectLayer[0].properties["background_color"]):
            # Convert the hex string to a rgb list
            color = [int(self.objectLayer[0].properties["background_color"][3:][i:i + 6 // 3], 16) for i in range(0, 6, 6 // 3)]
        else:
            # Set it to light blue
            color = [181, 227, 255]
        
        # Set the background color of the level
        arcade.set_background_color(color)

    def update_animation(self):
        """ This function is called every frame. It will loop through all of the animations for the player """

        # 'Flash' the player
        if(self.playerDying == True):
            self.playerSprite.texture = self.playerSprite.textures[floor(self.playerSprite.cur_texture_index % 2) + 5][self.playerSprite.direction]
            # Increment this so we know for how many frames the player has been dying for
            self.totalDyingFrames += 1

            # If the player has flashed white 5 times then reset some stuff and kill the player
            if(self.totalDyingFrames > 10 * FRAMES_PER_TEXTURE):
                self.playerDying = False
                self.totalDyingFrames = 0
                self.playerDead = True
                
        # If we can jump then it means we are walking on the ground
        elif(self.onGround == True):

            # If the player is standing still then we want the texture to be the middle-walking one, "sprites/walking_1.png"
            if(self.playerSprite.change_x == 0):
                self.playerSprite.cur_texture_index = 1
                self.playerSprite.texture = self.playerSprite.textures[1][self.playerSprite.direction]

            # Using a modulus operator is efficient because the cur_texture_index will be looped back to 0,
            # which prevents us having to check and reset it if it's on the boundary.
            # This creates the walking animation of the player
            else:
                self.playerSprite.texture = self.playerSprite.textures[floor(self.playerSprite.cur_texture_index % 3)][self.playerSprite.direction]

        # Self explanatory, checks if we're on a ladder
        elif(self.onLadder == True):
            # If we are not moving, display the 'idle' animation image
            if(self.playerSprite.change_y == 0):
                self.playerSprite.texture = self.playerSprite.textures[1][self.playerSprite.direction]
            # Do some math to get the correct texture. The +3 is needed becuase the climbing textures start at index 3 in the texture list
            else:
                self.playerSprite.texture = self.playerSprite.textures[floor(self.playerSprite.cur_texture_index % 2) + 3]

        # If we are in the air switch to "sprites/walking_2.png"
        else:
            self.playerSprite.cur_texture_index = 2
            self.playerSprite.texture = self.playerSprite.textures[2][self.playerSprite.direction]

    # Main menu
    def gameStartScreen(self):
        """
        Handles drawing when the game is on the main menu. There is a title and 5 buttons that are displayed
        """

        # Add the 5 buttons to buttons; Play game, Select level, Exit to desktop, How to play and mute/unmute. If the are you sure menu is being displayed, then we want the buttons to be draw-only
        self.buttons = [{"button": [SCREEN_WIDTH / 2 - 200, SCREEN_HEIGHT / 2, 160, 50, "Play game"], "function": buttonActions.play, "drawOnly": self.displayAreYouSure},
                        {"button": [SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 160, 50, "Select level"], "function": buttonActions.selectLevel, "drawOnly": self.displayAreYouSure},
                        {"button": [SCREEN_WIDTH / 2 + 200, SCREEN_HEIGHT / 2, 160, 50, "Exit to desktop"], "function": buttonActions.exitGame, "drawOnly": self.displayAreYouSure},
                        {"button": [SCREEN_WIDTH / 2 - 100, SCREEN_HEIGHT / 2 - 90, 160, 50, "Your mission"], "function": buttonActions.tutorial, "drawOnly": self.displayAreYouSure},
                        {"button": [SCREEN_WIDTH / 2 + 100, SCREEN_HEIGHT / 2 - 90, 160, 50, "Mute" if self.playMusic == True else "Unmute"], "function": buttonActions.flipMusicState, "drawOnly": self.displayAreYouSure}]

        arcade.draw_text("Platformer", SCREEN_WIDTH / 2, 3 * SCREEN_HEIGHT / 4, arcade.color.BLACK, 100, anchor_y="center", anchor_x="center")

        if(self.displayAreYouSure == True):
            # -- Draw the are you sure popup -- #

            # Rectangle background
            arcade.draw_rectangle_outline((SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom, 450, 180, arcade.color.BLACK, 2)
            arcade.draw_rectangle_filled((SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom, 450, 180, [255, 255, 255])

            arcade.draw_text("Are you sure?", (SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom + 45, arcade.color.BLACK, 28, anchor_x="center", anchor_y="center")
            
            # Add the Yes and No buttons to buttons
            self.buttons += [{"button": [SCREEN_WIDTH / 2 - 90, 3 * SCREEN_HEIGHT / 4 + self.viewportBottom - 40, 160, 50, "Yes"], "function": buttonActions.yes, "drawOnly": False},
                            {"button": [SCREEN_WIDTH / 2 + 90, 3 * SCREEN_HEIGHT / 4 + self.viewportBottom - 40, 160, 50, "No"], "function": buttonActions.no, "drawOnly": False}]

        # Draw the buttons. The second argument of drawButton is a boolean to determine if the button is pressed
        # or not, so we need to supply true if the mouse is pressed and if the mouse pointer is on the button and drawOnly is False
        for button in self.buttons:
            utils.drawButton(button["button"], self.mousePressed and not button["drawOnly"] and utils.isCoordInButton(self.mouse_x, self.mouse_y, button["button"]))

    def drawTextObjects(self):
        # Loop through all of the objects in the layer
        for tiledObject in self.objectLayer.tiled_objects:
            # If the object is a text object then we want to draw it
            if(tiledObject.type == "text"):

                print(tiledObject.properties)

                # Convert the hex color code to a rgb list
                color = [int(tiledObject.color[1:][i:i + 6 // 3], 16) for i in range(0, 6, 6 // 3)]

                print("321321321")
                # Draw the text with the parameters based on the object
                arcade.draw_text(tiledObject.text, tiledObject.location.x, tiledObject.location.y, color, tiledObject.font_size, tiledObject.size.width, tiledObject.horizontal_align, tiledObject.font_family, tiledObject.bold, tiledObject.italic, "left", "top", tiledObject.rotation)

    def gameRunScreen(self):
        """
        Handles drawing when the game is on the normal screen. It needs to draw the player, the levels and
        any buttons/menus that are currently being displayed.
        """

        # Draw the background image. I've stored the file number inside a property of the object in the object layer
        arcade.draw_lrwh_rectangle_textured(self.viewportLeft, self.viewportBottom, SCREEN_WIDTH, SCREEN_HEIGHT, self.backgrounds[self.objectLayer[0].properties["background"]])

        self.terrainList.draw()
        self.dangerList.draw()
        self.backgroundList.draw()
        self.ladderList.draw()

        #self.drawTextObjects()
        self.objectLayer.draw()

        self.playerSprite.draw()

        # Clear the button list becuase it might have buttons from the previous screen
        self.buttons = []

        # If the player is on the ending level then draw the text
        if(self.level == 0):
            arcade.draw_text("              Congratulations!\nYou have completed platformer!", 384, 320, arcade.color.TROPICAL_RAIN_FOREST, 30, anchor_x="center")
            arcade.draw_text("Main menu", 928, 325, arcade.color.BLAST_OFF_BRONZE, 18, anchor_x="center")

        if(self.displayAreYouSure == True):
            # -- Draw the are you sure popup -- #

            # Rectangle background
            arcade.draw_rectangle_outline((SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom, 450, 180, arcade.color.BLACK, 2)
            arcade.draw_rectangle_filled((SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom, 450, 180, arcade.color.WHITE)

            arcade.draw_text("Are you sure?", (SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom + 50, arcade.color.BLACK, 28, anchor_x="center", anchor_y="center")
            arcade.draw_text("your progress on this level will be lost", (SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom + 10, arcade.color.BLACK, 16, anchor_x = "center", anchor_y="center")

            # Add the yes and no buttons to the list
            self.buttons = [{"button": [SCREEN_WIDTH / 2 + self.viewportLeft - 90, 3 * SCREEN_HEIGHT / 4 + self.viewportBottom - 40, 160, 50, "Yes"], "function": buttonActions.yes, "drawOnly": False},
                            {"button": [SCREEN_WIDTH / 2 + self.viewportLeft + 90, 3 * SCREEN_HEIGHT / 4 + self.viewportBottom - 40, 160, 50, "No"], "function": buttonActions.no, "drawOnly": False}]

        elif(self.playerDead == True):
            # -- Draw the death message popup -- #

            # Rectangle background
            arcade.draw_rectangle_outline((SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom, 450, 180, arcade.color.BLACK, 2)
            arcade.draw_rectangle_filled((SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom, 450, 180, arcade.color.WHITE)
            
            # Death message text
            arcade.draw_text("You died!", (SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom + 45, arcade.color.BLACK, 28, anchor_x="center", anchor_y="center")

            # Add the respawn and main menu buttons to the buttons list
            self.buttons = [{"button": [SCREEN_WIDTH / 2 + self.viewportLeft - 90, 3 * SCREEN_HEIGHT / 4 + self.viewportBottom - 40, 160, 50, "Respawn"], "function": buttonActions.respawn, "drawOnly": False},
                            {"button": [SCREEN_WIDTH / 2 + self.viewportLeft + 90, 3 * SCREEN_HEIGHT / 4 + self.viewportBottom - 40, 160, 50, "Main menu"], "function": buttonActions.mainMenu, "drawOnly": False}]

        elif(self.paused == True):
            # -- Draw the paused menu -- #

            # Rectangle background
            arcade.draw_rectangle_outline((SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom - 35, 450, 250, arcade.color.BLACK, 2)
            arcade.draw_rectangle_filled((SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom - 35, 450, 250, arcade.color.WHITE)
            
            # Text
            arcade.draw_text("Paused", (SCREEN_WIDTH / 2) + self.viewportLeft, (3 * SCREEN_HEIGHT / 4) + self.viewportBottom + 45, arcade.color.BLACK, 28, anchor_x="center", anchor_y="center")
            
            # Add the 4 buttons to the button list. Restart level, resume, main menu and mute/unmute
            self.buttons = [{"button": [SCREEN_WIDTH / 2 + self.viewportLeft - 90, 3 * SCREEN_HEIGHT / 4 + self.viewportBottom - 30, 160, 50, "Restart level"], "function": buttonActions.restartLevel, "drawOnly": False},
                            {"button": [SCREEN_WIDTH / 2 + self.viewportLeft + 90, 3 * SCREEN_HEIGHT / 4 + self.viewportBottom - 30, 160, 50, "Resume"], "function": buttonActions.resume, "drawOnly": False},
                            {"button": [SCREEN_WIDTH / 2 + self.viewportLeft - 90, 3 * SCREEN_HEIGHT / 4 + self.viewportBottom - 100, 160, 50, "Main menu"], "function": buttonActions.mainMenuFromPause, "drawOnly": False},
                            {"button": [SCREEN_WIDTH / 2 + self.viewportLeft + 90, 3 * SCREEN_HEIGHT / 4 + self.viewportBottom - 100, 160, 50, "Mute" if self.playMusic == True else "Unmute"], "function": buttonActions.flipMusicState, "drawOnly": False}]

        # If there is no menu being drawn, and we're not on the ending level,
        # then we want to display the current level in the corner of the screen
        elif(self.level != 0):
            # Level display
            arcade.draw_text("Level: " + str(self.level), 20 + self.viewportLeft, SCREEN_HEIGHT - 40 + self.viewportBottom, arcade.color.BLACK, 20)

        # Draw the buttons. The second argument of drawButton is a boolean to determine if the button is pressed
        # or not, so we need to supply true if the mouse is pressed and if the mouse pointer is on the button and drawOnly is False
        for button in self.buttons:
            utils.drawButton(button["button"], self.mousePressed and not button["drawOnly"] and utils.isCoordInButton(self.mouse_x, self.mouse_y, button["button"]))

    def levelSelectionScreen(self):
        """
        Handles drawing when the game is on the level selection screen. The level icons (gates) need to be
        drawn, along with the button that goes back to the main menu
        """

        # Initialize. gateNum will be the same as the level that the gate links to
        gateNum = 1

        # Draw the text
        arcade.draw_text("Select level", SCREEN_WIDTH / 2, SCREEN_HEIGHT - 70, arcade.color.BLACK, 100, anchor_y="center", anchor_x="center")
        arcade.draw_text("Current progress: " + str(self.highestLevel) + "/" + str(LEVEL_COUNT), SCREEN_WIDTH / 2, SCREEN_HEIGHT - 150, arcade.color.BLACK, 25, anchor_x="center", anchor_y="center")

        # Add the main menu button to the list and draw it
        self.buttons = [{"button": [100, 50, 160, 50, "Main menu"], "function": buttonActions.leaveLvlSelectionScreen, "drawOnly": False}]
        utils.drawButton(self.buttons[0]["button"], self.mousePressed and utils.isCoordInButton(self.mouse_x, self.mouse_y, self.buttons[0]["button"]))

        # Loop through all of the coordinates that will be good for drawing a gate
        for gateYCoord in range(SCREEN_HEIGHT - 230, 0, -140):
            for gateXCoord in range(128, SCREEN_WIDTH, 128):
                # If the amount of gates is going to exceed the amount of levels then we need to exit the function
                if(gateNum > LEVEL_COUNT):
                    return
                
                # Set the coordinates of the sprite
                self.gateSprite.center_x = gateXCoord
                self.gateSprite.center_y = gateYCoord

                # If the gate number is less than or equal to the highest level the player has accessed, then
                # use an open gate texture.
                if(gateNum <= self.highestLevel):
                    # If the mouse is over the gate then draw it glowing
                    if(self.gateSprite.collides_with_point([self.mouse_x, self.mouse_y])):
                        self.gateSprite.texture = self.gateSprite.glowingTexture
                    else:
                        self.gateSprite.texture = self.gateSprite.openTexture

                # Otherwise use a closed gate texture
                else:
                    self.gateSprite.texture = self.gateSprite.closedTexture

                # Draw it
                self.gateSprite.draw()
                # Draw the level number
                arcade.draw_text(str(gateNum), self.gateSprite.center_x, self.gateSprite.center_y + 30, arcade.color.BLAST_OFF_BRONZE, 18, anchor_x="center")

                gateNum += 1

    def loadingScreen(self):
        """ Handles drawing of the loading screen. Draws the text and a loading bar with progress percentage underneath"""

        # Text
        arcade.draw_text("Loading...", SCREEN_WIDTH / 2, 3 * SCREEN_HEIGHT / 4, arcade.color.BLACK, 70, anchor_x="center", anchor_y="center")
        
        # Loading bar. This is based on the value of self.loadingProgress
        arcade.draw_rectangle_outline(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 806, 26, arcade.color.BLACK, 2)
        arcade.draw_rectangle_filled(SCREEN_WIDTH / 2 - 400 + (800 * self.loadingProgress / 2), SCREEN_HEIGHT / 2, 800 * self.loadingProgress, 20, arcade.color.BLACK)
        
        # Draw the percentage
        arcade.draw_text(f"{round(self.loadingProgress * 100)}%", SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 18, arcade.color.BLACK, 20, anchor_x="center", anchor_y="top")

    def tutorialScreen(self):
        """
        Handles drawing the instructions. This will display the heading, the instructions, the sprites 
        showing the player what things look like and the main menu button
        """

        # Draw the heading and instructions text
        arcade.draw_text("Instructions", SCREEN_WIDTH / 2, SCREEN_HEIGHT - 70, arcade.color.BLACK, 100, anchor_y="center", anchor_x="center")
        arcade.draw_text("""Use either the arrow keys or w,a,s,d to move.

W or UP  ->  jump / climb up ladder
A or LEFT  ->  move left
S or DOWN  ->  climb down ladder
D or RIGHT  ->  move right
Esc  ->  pause/resume
                            
The space key can also be used for jumping

Dont touch the lava                   or the squish monsters™
and get to the end without falling off the world.



When you go through one of these
you will be transported to the next level.

Good luck!""", SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 40, arcade.color.BLACK, 18, anchor_x="center", anchor_y="center")

        # Draw the gate sprite for demonstration
        self.gateSprite.center_x = SCREEN_WIDTH / 2 + 115
        self.gateSprite.center_y = SCREEN_HEIGHT / 2 - 130
        self.gateSprite.draw()

        # Draw the danger sprite for demonstration at the correct coordinates with all of its 3 textures
        self.dangerSprite.center_y = SCREEN_HEIGHT / 2 - 50
        for i in range(0, 3):
            self.dangerSprite.texture = self.dangerSprite.textures[i]

            # Do some math to get the correct coordinates. These values can be obtained using quadratic regression.
            # It needs to be quadratic regression instead of linear regression, because the squish monster™ is further apart then the lava images.
            self.dangerSprite.center_x = SCREEN_WIDTH / 2 + ((118 * i**2) - (76 * i) - 45)
            self.dangerSprite.draw()

        # Add the main menu button to the list and draw it
        self.buttons = [{"button": [100, 50, 160, 50, "Main menu"], "function": buttonActions.leaveLvlSelectionScreen, "drawOnly": False}]
        utils.drawButton(self.buttons[0]["button"], self.mousePressed and utils.isCoordInButton(self.mouse_x, self.mouse_y, self.buttons[0]["button"]))

    def on_draw(self):
        """
        Render the screen.
        """

        # This command should happen before we start drawing. It will clear
        # the screen to the background color, and erase what we drew last frame.
        arcade.start_render()

        # Decide on which screen the game is currently on, and call the correct drawing function
        if(self.screenType == 0):
            self.gameStartScreen()
        elif(self.screenType == 1):
            self.gameRunScreen()
        elif(self.screenType == 2):
            self.levelSelectionScreen()
        elif(self.screenType == 3):
            self.loadingScreen()
        else:
            self.tutorialScreen()

    def on_update(self, delta_time):
        """
        All the logic to move, and the game logic goes here.
        Normally, you'll call update() on the sprite lists that
        need it.
        """

        # Print the fps for debugging
        #print("FPS: " + str(1/delta_time))

        # Update mouse position by subtracting the location of the window
        # from the position on-screen, and then adding the viewport
        # The y part of a screen coordinate starts at 0 from the TOP of the screen, whereas the in-game coordinates
        # start at 0 from the BOTTOM of the window.
        # Thats why we need to flip the Y-coordinate upside down
        self.mouse_x = mouse.position[0] - super().get_location()[0] + self.viewportLeft
        self.mouse_y = SCREEN_HEIGHT - (mouse.position[1] - super().get_location()[1]) - 1 + self.viewportBottom

        # We only want to run the rest of update code if the user is on the normal game screen.
        # The main menu and other menus like pause are static
        if(self.screenType != 1 or self.playerDead == True or self.paused == True or self.displayAreYouSure == True):
            return

        # Need to update the texture index because of the dying/flashing animation
        # There needs to be two self.update_animation()'s because when we exit from the dying one, the rest of the update code should not run
        if(self.playerDying == True):
            self.playerSprite.cur_texture_index += 1 / FRAMES_PER_TEXTURE
            self.update_animation()
            return
        else:
            # Update the player's animations
            self.update_animation()

        ## ---- HANDLE PLAYER MOVEMENT ---- ##

        # Update the movement states of the player
        self.movementStates = utils.resolveMovementStates(self.keyStates)

        # Set these variables for a slight speed increase
        self.onGround = self.physicsEngine.can_jump()
        self.onLadder = self.physicsEngine.is_on_ladder()


        # -- ladder movement -- #

        if(self.onLadder == True):
            # If both keys are being pressed at the same time then the player sprite should stop moving
            if(self.movementStates["up"] == "pressed" and self.movementStates["down"] == "pressed"):
                self.playerSprite.change_y = 0
            # Move up
            elif(self.movementStates["up"] == "pressed"):
                self.playerSprite.change_y = 3
                self.playerSprite.cur_texture_index += 1 / FRAMES_PER_TEXTURE
            #Move down
            elif(self.movementStates["down"] == "pressed"):
                self.playerSprite.change_y = -3
                self.playerSprite.cur_texture_index += 1 / FRAMES_PER_TEXTURE

        # if we are just on the ground then we can jump normally
        elif(self.movementStates["up"] == "pressed" and self.onGround == True):
            self.physicsEngine.jump(9)

        # -- side to side movement -- #
        
        # If both keys are being pressed at the same time, or if neither of the keys are being pressed, then the player sprite shouldn't move
        if(self.movementStates["left"] == "pressed" and self.movementStates["right"] == "pressed" or
           self.movementStates["left"] == "not_pressed" and self.movementStates["right"] == "not_pressed"):
            self.playerSprite.change_x = 0

        # Otherwise set the horizontal velocity to 3, the direction to 0 (for the animation code)
        elif(self.movementStates["right"] == "pressed"):
            self.playerSprite.change_x = 3
            self.playerSprite.direction = 0

            # If we are touching the ground then update the texture index. This prevents the climbing animation
            # from running if we walk sideways on a ladder
            if(self.onGround == True):
                self.playerSprite.cur_texture_index += 1 / FRAMES_PER_TEXTURE

        #set the horizontal velocity to -3, the direction to 1 (for the animation code)
        elif(self.movementStates["left"] == "pressed"):
            self.playerSprite.change_x = -3
            self.playerSprite.direction = 1

            # If we are touching the ground then update the texture index. This prevents the climbing animation
            # from running if we walk sideways on a ladder
            if(self.onGround == True):
                self.playerSprite.cur_texture_index += 1 / FRAMES_PER_TEXTURE

        ## ---- HANDLE PLAYER MOVEMENT (closing tag)---- ##



        # If we are touching a ladder and the player doesn't want to move, then we should just be still
        # Even though arcade's basic physics engine already handles touching ladders, I added this because
        # I don't like their idea of ladder movement.
        # They think you should move even if you're not doing anything. Eww.
        if(self.onLadder == True and self.movementStates["up"] == "not_pressed" and self.movementStates["down"] == "not_pressed"):
            self.playerSprite.change_y = 0

        # Resolve collisions and move the player
        self.physicsEngine.update()
        
        # If the player fell off the map then kill the player
        if(self.playerSprite.bottom < -2000):
            self.playerDead = True
            self.audioEngine.stopStart(True, self.playMusic)

        # If the player touched any dangerous tiles let the game know to flash the player.
        # Also exit with return so we don't scroll or do anything else in  on_update()
        elif(self.playerSprite.collides_with_list(self.dangerList)):
            self.playerDying = True
            self.audioEngine.stopStart(True, self.playMusic)
            return
        
        # ⬆⬆ When the player is dead the music should also be paused. We can use this to seek back to the start if the user wants to respawn ⬆⬆

        # --- do scrolling --- #

        # For a better effect, don't scroll when the player has fallen below the lowest points on the map
        if(self.playerSprite.top < 0):
            return

        # This will be set to true when the viewport needs to be changed, so we know when to update the viewport.
        # Doing this prevents having to set the viewport every frame
        viewportChanged = False

        # If the distance between the top of the player and the top of the viewport is less than the minimum margin
        # then we need to calculate how much we need to change the viewport by
        if((self.viewportBottom + SCREEN_HEIGHT) - self.playerSprite.top < PLAYER_SPRITE_TOP_MARGIN):
            self.viewportBottom += (self.playerSprite.top + PLAYER_SPRITE_TOP_MARGIN) - (self.viewportBottom + SCREEN_HEIGHT)
            viewportChanged = True

        #+ If the distance from the player's left side to the left side of the viewport is less than the minimum margin
        # then we need to calculate how much we need to change the viewport by
        if(self.playerSprite.left - self.viewportLeft < PLAYER_SPRITE_LR_MARGIN):
            self.viewportLeft -= PLAYER_SPRITE_LR_MARGIN - (self.playerSprite.left - self.viewportLeft)
            viewportChanged = True

        #+ If the distance from the player's right side to the right side of the viewport is less than the minimum margin
        # then we need to calculate how much we need to change the viewport by
        if((self.viewportLeft + SCREEN_WIDTH) - self.playerSprite.right < PLAYER_SPRITE_LR_MARGIN):
            self.viewportLeft += (self.playerSprite.right + PLAYER_SPRITE_LR_MARGIN) - (self.viewportLeft + SCREEN_WIDTH)
            viewportChanged = True

        # If the distance between the bottom of the player and the bottom of the viewport is less than the minimum margin,
        # then we need to calculate how much we need to change the viewport by
        if(self.playerSprite.bottom - self.viewportBottom < PLAYER_SPRITE_BOTTOM_MARGIN):
            decrementValue = PLAYER_SPRITE_BOTTOM_MARGIN - (self.playerSprite.bottom - self.viewportBottom)
            if(self.viewportBottom - decrementValue >= 0):
                self.viewportBottom -= decrementValue
                viewportChanged = True


        # Now we can set the updated viewport (if it has been updated)
        if(viewportChanged == True):
            # Only scroll to integers. Otherwise we end up with pixels that don't line up on the screen
            self.viewportBottom = int(self.viewportBottom)
            self.viewportLeft = int(self.viewportLeft)

            # set the viewport so we can scroll
            arcade.set_viewport(self.viewportLeft, self.viewportLeft + SCREEN_WIDTH, self.viewportBottom, self.viewportBottom + SCREEN_HEIGHT)


        # -- Handle level completions -- #

        # If the center of the player is within the boundaries of the finishing coordinates,
        # then we can go to the next level. The finishing coordinates are stored in the properties of an object
        # on the current object layer. These can be changed in the tiled map editor
        if( self.playerSprite.center_x > self.objectLayer[0].properties["X"]
        and self.playerSprite.center_x < self.objectLayer[0].properties["X"] + 64
        and self.playerSprite.center_y > self.objectLayer[0].properties["Y"]
        and self.playerSprite.center_y < self.objectLayer[0].properties["Y"] + 64):
            
            # If the player has finished the platformer then we need to go to the ending level which is lvl 0
            if(self.level == LEVEL_COUNT):
                self.level = 0
            # If the player has finished the ending level, then set the level to 1 but go to the main menu
            elif(self.level == 0):
                self.level = 1
                self.screenType = 0

                # Stop the gameplay music, and switch it to the menu music
                self.audioEngine.play(self.audioEngine.musicTypes.MENU, self.playMusic)

                # Write the new data into the file
                utils.writeUserData(self)
            
            # Business as usual otherwise
            else:
                # Next level
                self.level += 1

                # Write the new data into the file
                utils.writeUserData(self)

            # Reset the player, coordinates and setup the new level
            self.resetPlayer()
            self.setupLevel()

    def on_key_press(self, key, key_modifiers):
        """
        Called whenever a key on the keyboard is pressed.
        """

        # Update key state if it is a key we are concerned with
        if(key in self.keyStates):
            self.keyStates[key] = "pressed"

    def on_key_release(self, key, key_modifiers):
        """
        Called whenever the user releases a previously pressed key.
        """

        # Update key state if it is a key we are concerned with
        if(key in self.keyStates):
            self.keyStates[key] = "not_pressed"

        # If the escape key was released then we want to show (or hide) the pause menu
        # This should only happen if the game is on the normal gameplay screen, and if there aren't any other additional menus
        elif(key == 65307 and self.screenType == 1 and self.displayAreYouSure == False and self.playerDead == False):
            # If the pause menu is already being shown, then resume the game
            if(self.paused == True):
                buttonActions.resume(self)
            # Otherwise we can show it
            else:
                self.paused = True
                # Pause the music
                self.audioEngine.stopStart(True, self.playMusic)

    def on_mouse_press(self, x, y, button, modifiers):
        """
        Called whenever the user presses a button on the mouse
        """

        # Set the variable so the rest of the ggame knows that the mouse is pressed
        if(button == arcade.MOUSE_BUTTON_LEFT):
            self.mousePressed = True

    def on_mouse_release(self, x, y, mouse_button, modifiers):
        """
        Called when the user presses a button on the mouse
        """

        # If the button wasn't the left mouse button, then we can exit since we don't need anything else
        if(mouse_button != arcade.MOUSE_BUTTON_LEFT):
            return

        self.mousePressed = False

        # Loop through all of the buttons. If the mouse was released inside the button, then we can call the
        # button's action function that is stored in the "function" key
        for button in self.buttons:
            if(utils.isCoordInButton(self.mouse_x, self.mouse_y, button["button"]) and button["drawOnly"] == False):
                button["function"](self)

        # Handle mouse releases on the level selection screen
        if(self.screenType == 2):
            gateNum = 1

            # Just like the level selection screen, loop through all of the coordinates where a gate sprite is
            for gateYCoord in range(SCREEN_HEIGHT - 230, 0, -128):
                for gateXCoord in range(128, SCREEN_WIDTH, 128):
                    # If the amount of gates is going to exceed the amount of total levels, or exceed 
                    # the highest level the player has accessed, then we need to exit the function
                    if(gateNum > LEVEL_COUNT or gateNum > self.highestLevel):
                        return
                
                    # Set the coordinates of the sprite for checking mouse releases
                    self.gateSprite.center_y = gateYCoord
                    self.gateSprite.center_x = gateXCoord

                    # If the mouse was released inside the level icon
                    if(self.gateSprite.collides_with_point([x, y])):
                        # Set the correct level
                        self.level = gateNum

                        # This will be just like clicking the play button. The screen type will change, the level will be setup and the background usic will start
                        buttonActions.play(self)
                        return

                    gateNum += 1

    def resetPlayer(self):
        """
        This function will reset the player's coordinates to the spawn point,
        switch their movement vectors back to 0, and reset the viewport
        """

        # Reset player location to spawn point
        self.playerSprite.center_x = 96
        self.playerSprite.center_y = 96

        #Reset player movement vectors
        self.playerSprite.stop()

        # Reset viewport, and the variables used to keep track of the viewport
        self.viewportBottom = 0
        self.viewportLeft = 0
        arcade.set_viewport(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT)

if __name__ == "__main__":
    # Set the process to be DPI-aware so that DPI scaling doesn't affect the values we get for the screen size
    # This was helpful: https://stackoverflow.com/a/3129524
    ctypes.windll.user32.SetProcessDPIAware()

    # The height and width of the app should be slightly smaller than the size of the users screen, hence the -100
    SCREEN_WIDTH = ctypes.windll.user32.GetSystemMetrics(0) - 100
    SCREEN_HEIGHT = ctypes.windll.user32.GetSystemMetrics(1) - 100

    # Create an instance of Platformer class. This will start the window
    game = Platformer(SCREEN_WIDTH, SCREEN_HEIGHT, "Platformer")

    # Start loading the assets on a separate thread
    thread = Thread(target = utils.loadAssets, args = (game, ))
    thread.start()

    arcade.run()   # arcade.run() will start the main window and loop, and listen for events

    # If there is a stream
    if(game.audioEngine.stream):
        # Stop playing the stream.
        game.audioEngine.stream.stop_stream()

        # Close the stream to free resource
        game.audioEngine.stream.close()

        # Close the file stream to the current music file.
        # I think the wav module does this anyway, but I'm just doing it here to be safe
        game.audioEngine.musicFile.close()

    # Terminate PyAudio to free resources
    game.audioEngine.audio.terminate()