import pyaudio
import wave

class AudioEngine:
    """
    Class for handling everything to do with audio. This class makes it easy to implement and efficient.
    """

    # -- CONSTANT -- #
    # When you call play() you need to supply one of these types of music. Currently there are only 2: MENU and BACKGROUND
    musicTypes = obj = type("obj", (object,), {"MENU": 0, "BACKGROUND": 1})

    def __init__(self):
        # Initialize PyAudio
        self.audio = pyaudio.PyAudio()

        # Initialize the variables
        self.stream = None
        self.musicFile = None
        self.musicFileLength = 0

    def callback(self, in_data, frame_count, time_info, status):
        """ This function will be called by the stream. It reads the music flie and passes the audio data back """

        # Read the data from the wav file
        data = self.musicFile.readframes(frame_count)

        # If the file pointer has read the same amount of bytes that are in the file, then we must be at the end. We need to restart the track
        if(self.musicFile.tell() == self.musicFileLength):
            # Go back to the start of the file
            self.musicFile.rewind()
            # Read in the new data
            data = self.musicFile.readframes(frame_count)

        # Pass the data back to the stream
        return (data, pyaudio.paContinue)

    def play(self, musicType, playMusic):
        """
        Call this to play music. If music is already playing, then it will stop the current music before playing the new track.
        play() also exits if the user doesn't want to play music. This makes it easier to use since we don't have to have small if-statemetns everywhere
        """
       
        # If the user has music off then don't play any music
        if(playMusic == False):
            return

        # If the music is new music, and there is actually old music playing, then close the old music.
        # Since self.stream could also be of type None at the start, we need to add the self.stream check
        if(self.stream and self.stream.is_active() == True):
            self.stream.stop_stream()
            self.musicFile.close()

        # Select the correct wav file
        # The file stream will be saved to the musicFile variable
        if(musicType == self.musicTypes.MENU):
            self.musicFile = wave.open("sounds/menus.wav", "rb")
        elif(musicType == self.musicTypes.BACKGROUND):
            self.musicFile = wave.open("sounds/background.wav", "rb")

        # Store the length of the file so that the callback will know when to restart the track
        self.musicFileLength = self.musicFile.getnframes()
        
        # If the stream doesn't exist, then this is the first time calling play(). We need to create the stream for later use as well.
        if(not self.stream):
            self.stream = self.audio.open(format=self.audio.get_format_from_width(self.musicFile.getsampwidth()), channels=self.musicFile.getnchannels(), rate=self.musicFile.getframerate(), output=True, stream_callback=self.callback)
        
        # Start the stream
        self.stream.start_stream()

    def stopStart(self, stop, playMusic):
        """
        This function can be used to pause and resume the music, or just stop it which is effectively the same thing.
        It also has the playMusic == False check so we don't have to type the if-statements whenever we want to pause
        """

        # Exit if the user doesn't have music on.
        if(playMusic == False):
            return
        
        # If the supplied value for stop is True, then we want to pause the music
        if(stop == True):
            self.stream.stop_stream()
        # Otherwise the music should be resumed
        else:
            self.stream.start_stream()