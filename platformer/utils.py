import arcade
from time import sleep
from threading import Thread

ASSET_COUNT = 65
LEVEL_COUNT = 6

def drawButton(button, pressed):
    """
    This function will draw a button on the screen. Make sure it's called after arcade.start_render().
    the button parameter is a list containing the button data: [center_x, center_y, width, height, text]
    If pressed is True, then the button will be drawn as pressed.
    """

    # Draw button outline
    arcade.draw_rectangle_outline(button[0], button[1], button[2], button[3], arcade.color.BLACK, 2)

    # If the button is pressed then draw it filled in
    if(pressed == True):
        arcade.draw_rectangle_filled(button[0], button[1], button[2] - 5, button[3] - 5, [245, 245, 245])  

    # Draw the button text
    arcade.draw_text(button[4], button[0], button[1], arcade.color.BLACK, font_size = 18, font_name = "Arial",
                    width = button[2], align="center", anchor_x="center", anchor_y="center")

# Button is a list: [center_x, center_y, width, height, text]
def isCoordInButton(x, y, button):
    """ Checks if the given coordinates are inside the provided button """

    # If any of these statements are true, then the coordinate is too far outside the button in 1 direction.
    if x > button[0] + button[2] / 2:
        return False
    if x < button[0] - button[2] / 2:
        return False
    if y > button[1] + button[3] / 2:
        return False
    if y < button[1] - button[3] / 2:
        return False

    # If they're all false then the coordinate is inside the button, return True
    return True

def createLayer(tmxMap, layerName, game, isObjectLayer):
    """
    Takes a map object and a layer name, and parses out a sprite list from the layer.
    This is needed to actually draw the layer. If the layer is an object layer, then we dont want to draw it,
    so createLayer will just return the layer object
    """

    # If it's not an object layer, process the layer and get a sprite list
    if(isObjectLayer == False):
        # Initialize the sprite list that holds the layer. This layer is static, so initializing this list as static will speed up drawing.
        # Additionally, we cna use spatial hashing on the sprite list. This speeds up collision detection ALOT.
        layer = arcade.SpriteList(True, 64, True)

        # Get the layer
        initialLayer = arcade.tilemap.process_layer(tmxMap, layerName)

        # Loop through every tile in the layer
        for tile in initialLayer:
            # Put the tile/sprite into the sprite list
            layer.append(tile)

    # Otherwise, return the layer object
    else:
        layer = arcade.tilemap.get_tilemap_layer(tmxMap, layerName)

    # Increment the loading bar for accurate display of loading progress
    game.loadingProgress += 1/ASSET_COUNT

    return layer

def loadTexturePair(filename, game):
    """
    For loading a texture pair, one of which is mirrored.
    We can use this other texture for when the sprite changes direction
    """

    # Load the first texture from the given filename
    texture1 = arcade.load_texture(filename)
    game.loadingProgress += 1/ASSET_COUNT

    # The second texture will eb the same as the first texture, but arcade will load it mirrored.
    # This means that is an exact copy, except reversed. It's actually quite a useful feature, since I don't have to create a whole new image
    texture2 = arcade.load_texture(filename, mirrored=True)
    game.loadingProgress += 1/ASSET_COUNT

    return [texture1, texture2]

def loadAssets(game):
    """
    This will load all of the assets for the game, and store them into the properties of the object that is passed
    to this function. These assets include all of the levels and the layers, all of the animations for the player,
    all of the textures for the gate sprite and the 3 textures for the danger sprite that show the player which
    tiles are dangerous. After each asset is loaded, game.loaingProgress will be incremented so the main thread
    knows how much has been loaded.
    """

    # Loop through all of the levels
    for i in range(0, LEVEL_COUNT + 1):
        # Read in the level
        level = arcade.tilemap.read_tmx(f"maps/level {i}.tmx")
        game.loadingProgress += 1/ASSET_COUNT

        # Parse sprite lists from all of the layers in the level
        platforms = createLayer(level, "platforms", game, False)
        ladders = createLayer(level, "ladders", game, False)
        danger = createLayer(level, "danger", game, False)
        background = createLayer(level, "background", game, False)
        # I'm treating this like a normal layer for now, since full object parsing hasn't yet been added
        objects = createLayer(level, "objects", game, False)

        # Put all of the data into the levels list
        game.levels.append({"platforms": platforms, "ladders": ladders, "danger": danger, "background": background, "objects": objects})
    
    # Load the player sprite, using its first texture
    game.playerSprite = arcade.Sprite("sprites/sprite_walking_0.png")

    # Set the player sprites position to the spawn point and add it to the sprite list
    game.playerSprite.bottom = 96
    game.playerSprite.left = 96

    # 0 means the player is moving right, 1 means they are moving left
    game.playerSprite.direction = 0

    game.loadingProgress += 1/ASSET_COUNT

    # Load all of the player sprites walking animations
    for t in range(0, 3):
        texturePair = loadTexturePair(f"sprites/sprite_walking_{t}.png", game)
        game.playerSprite.textures.append(texturePair)

    # Delete the first texture that we had to use to load the sprite, since it is not mirrored and not useful
    game.playerSprite.cur_texture_index = 1
    game.playerSprite.texture = game.playerSprite.textures[1][game.playerSprite.direction]
    del game.playerSprite.textures[0]

    # Load the player sprites climbing animations
    for t in range(0, 2):
        game.playerSprite.textures.append(arcade.load_texture(f"sprites/sprite_climbing_{t}.png"))
        game.loadingProgress += 1/ASSET_COUNT

    # Load the player sprites dying animations
    for t in range(0, 2):
        texturePair = loadTexturePair(f"sprites/sprite_dying_{t}.png", game)
        game.playerSprite.textures.append(texturePair)

    # Load the gate sprite for the level selection screen and the tutorial screen
    game.gateSprite = arcade.Sprite("sprites/gate.png")
    game.loadingProgress += 1/ASSET_COUNT
    
    # This is what will be displayed when a level is unlocked
    game.gateSprite.openTexture = arcade.load_texture("sprites/gate.png")
    game.loadingProgress += 1/ASSET_COUNT
    
    # This is a locked level which the user cannot access
    game.gateSprite.closedTexture = arcade.load_texture("sprites/lockedGate.png")
    game.loadingProgress += 1/ASSET_COUNT
    
    # This is a glowing gate that displays when the user hovers over it
    game.gateSprite.glowingTexture = arcade.load_texture("sprites/glowingGate.png")
    game.loadingProgress += 1/ASSET_COUNT
    
    # Load the danger sprite for the tutorial screen. This first texture will be the lava-wave image
    game.dangerSprite = arcade.Sprite("maps/32x32PlatformerTileset/sprite_38.png")
    game.loadingProgress += 1/ASSET_COUNT

    # Load the second texture for the danger sprite. This texture will be the other lava image.
    game.dangerSprite.textures.append(arcade.load_texture("maps/32x32PlatformerTileset/sprite_45.png"))
    game.loadingProgress += 1/ASSET_COUNT

    # Load the third texture for the danger sprite used in the tutorial screen. This is the image of the squish monster thing
    game.dangerSprite.textures.append(arcade.load_texture("maps/32x32PlatformerTileset/sprite_56.png"))
    game.loadingProgress += 1/ASSET_COUNT

    game.backgrounds = []
    for i in range(0, 3):
        game.backgrounds.append(arcade.load_texture(f"maps/background_images/background_{i}.png"))
        game.loadingProgress += 1 / ASSET_COUNT

    # Once we reach 100% sleep for half a second for effect
    sleep(0.5)

    # Play the menu music
    game.audioEngine.play(game.audioEngine.musicTypes.MENU, game.playMusic)

    # Go to the main menu
    game.screenType = 0

def resolveMovementStates(keyStates):
    movementStates = {}

    # If any of the up keys have been pressed, then make the 'up' movement state 'pressed'
    if(keyStates[32] == "pressed" or keyStates[65362] == "pressed" or keyStates[119] == "pressed"):
        movementStates["up"] = "pressed"
    else:   #+Otherwise reset
        movementStates["up"] = "not_pressed"

    # If any of the down keys have been pressed, then make the 'down' movement state 'pressed'
    if(keyStates[65364] == "pressed" or keyStates[115] == "pressed"):
        movementStates["down"] = "pressed"
    else:   #+Otherwise reset
        movementStates["down"] = "not_pressed"

    # If any of the left keys have been pressed, then make the 'left' movement state 'pressed'
    if(keyStates[65361] == "pressed" or keyStates[97] == "pressed"):
        movementStates["left"] = "pressed"
    else:   #+Otherwise reset
        movementStates["left"] = "not_pressed"

    # If any of the right keys have been pressed, then make the 'right' movement state 'pressed'
    if(keyStates[65363] == "pressed" or keyStates[100] == "pressed"):
        movementStates["right"] = "pressed"
    else:   #+Otherwise reset
        movementStates["right"] = "not_pressed"

    return movementStates

def writeUserData(game):
    # Write the current level, highest level and music state into the file
    with open("userData.txt", "w") as userData:
        userData.write(str(game.level) + "\n" + str(game.highestLevel) + "\n" + str(game.playMusic))