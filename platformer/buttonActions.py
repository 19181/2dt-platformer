"""
These functions will be called when the user presses a button.
They do the action (even though it's not much action). I have to have these function because python's
stupid lambda functions only let you have 1 statement.
The game parameter should be a Platformer instance so the functions can do their work
"""

from arcade import close_window
from arcade import set_background_color
import utils

def play(game):
    # Go to the game run screen so the player can play the game
    game.screenType = 1

    # Stop the menu music and play the background/gameplay music
    game.audioEngine.play(game.audioEngine.musicTypes.BACKGROUND, game.playMusic)

    # Setup the users current level
    game.setupLevel()

def selectLevel(game):
    # Set the screen to level selection
    game.screenType = 2

def exitGame(game):
    # Display the are you sure menu. This will prevent accidental closures of the game by the user
    game.displayAreYouSure = True
    game.displayAreYouSure_Reason = "exit_game"

def yes(game):
    # If the user clicked yes to exit the game, close the app
    if(game.displayAreYouSure_Reason == "exit_game"):
        close_window()
        return
    
    # Reset these two so that the menus don't show up once the user restarts the level,
    # or goes to the main menu and then comes back to the level
    game.displayAreYouSure = False
    game.paused = False

    # Reset the player and coordinates
    game.resetPlayer()

    # If the reason for pressing yes was to go back to the main menu then swicth the screen type
    if(game.displayAreYouSure_Reason == "main_menu"):
        game.screenType = 0

        # Stop the gameplay music, and switch it to the menu music
        game.audioEngine.play(game.audioEngine.musicTypes.MENU, game.playMusic)

        # Set the background color back to light blue
        set_background_color([181, 227, 255])
    
    # Otherwise if the player wants to restart the level then we need to restart the music. Everything else has already been done.
    else:
        game.audioEngine.play(game.audioEngine.musicTypes.BACKGROUND, game.playMusic)

def no(game):
    # Hide the are you sure menu
    game.displayAreYouSure = False

def leaveLvlSelectionScreen(game):
    # Change the screen type to the main menu
    game.screenType = 0

def respawn(game):
    # Reset everything and then make the player alive again so the level updates
    game.resetPlayer()
    game.playerDead = False

    # Restart the music by playing it again
    game.audioEngine.play(game.audioEngine.musicTypes.BACKGROUND, game.playMusic)

def mainMenu(game):
    # Reset everything and then switch the screen type to the main menu.
    # Also set the player to alive so that when the user switches back to the level, the respawn menu wont show
    game.resetPlayer()
    game.screenType = 0
    game.playerDead = False

    # Stop the gameplay music, and switch it to the menu music
    game.audioEngine.play(game.audioEngine.musicTypes.MENU, game.playMusic)

    # Set the background color back to light blue
    set_background_color([181, 227, 255])

def restartLevel(game):
    # Display the are you sure menu so users don't accidentally restart the level
    game.displayAreYouSure = True
    game.displayAreYouSure_Reason = "restart_level"

def resume(game):
    # Hide the pause menu
    game.paused = False

    # Resume the music
    game.audioEngine.stopStart(False, game.playMusic)

def mainMenuFromPause(game):
    # Display the are you sure menu so the user doesn't accidentally exit their level
    game.displayAreYouSure = True
    game.displayAreYouSure_Reason = "main_menu"

def tutorial(game):
    # Set the screen to tutorial screen
    game.screenType = 4

def flipMusicState(game):    
    # If the music was playing then the user wants to mute it
    if(game.playMusic == True):
        # Stop the music
        game.audioEngine.stopStart(True, game.playMusic)
        
        # Flip the state
        game.playMusic = False

    # If the music wasn't playing then the user wants to turn it on
    else:
        # Flip the state of game.playMusic. This needs to be done before the calls to the audio engine
        # so that they actually run the code without exiting
        game.playMusic = True

        # Play the new music. Since screenType will have a value of 1 when on the gameplay screen, and a value of 0 when on the main menu,
        # we can conveniently just pass it straight to play() because the values correspond exactly with the AudioEngine music type constants
        game.audioEngine.play(game.screenType, game.playMusic)

        # If the current screen type is 1 (the normal game screen) then the user must be on the pause menu because that is the only way to mute
        # or unmute the music from that screen. This means we need to start the music paused.
        if(game.screenType == 1):
            # Pause the music
            game.audioEngine.stopStart(True, game.playMusic)

    # Write the new data to the file
    utils.writeUserData(game)